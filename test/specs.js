var expect = require('expect.js');
var sinon = require('sinon');
var validator = require('validator');

var toBoolean = require('../sources/toBoolean.js');
var toInt = require('../sources/toInt.js');
var toFloat = require('../sources/toFloat.js');

var spy = validator;
spy.toBoolean = sinon.spy(validator, 'toBoolean');
spy.toInt = sinon.spy(validator, 'toInt');
spy.toFloat = sinon.spy(validator, 'toFloat');

describe('How to use sanitizer methods', function() {

    it('The method toBoolean() must be invoked', function() {
        var result = toBoolean(validator);
        expect(spy.toBoolean.called).to.be.ok();
        expect(result).to.be.a('boolean');
    });

    it('The method toInt() must be invoked', function() {
        var result = toInt(validator);
        expect(spy.toInt.called).to.be.ok();
        expect(result).to.be.a('number');
    });

    it('The method toFloat() must be invoked', function() {
        var result = toFloat(validator);
        expect(spy.toFloat.called).to.be.ok();
        expect(result).to.be.a('number');
    });
});